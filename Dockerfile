FROM registry.gitlab.com/asxa86/images/android-build:latest

# Install emulation tools for android.
RUN /android-sdk/cmdline-tools/latest/bin/sdkmanager "emulator"
RUN /android-sdk/cmdline-tools/latest/bin/sdkmanager "system-images;android-33;google_apis;x86_64"
RUN apt install -y libpulse-dev qemu-kvm libvirt-daemon-system libvirt-clients bridge-utils

# Run custom script to ensure /sbin/init is PID 1.
# https://code.visualstudio.com/remote/advancedcontainers/start-processes
# https://unix.stackexchange.com/questions/751600/can-i-call-sbin-init-from-init-script
COPY entrypoint.sh /
RUN chmod +x /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
CMD ["/sbin/init"]
