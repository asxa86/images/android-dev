#!/usr/bin/env bash

echo "Starting Android Development Environment"

# Initialize systemd.
exec "$@" &

# Keep the container running for vscode.
sleep infinity
